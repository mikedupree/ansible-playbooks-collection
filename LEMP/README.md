# Install LEMP stack on ubuntu server.


## About this repo

#### hosts 
- It's an inventory file that contains pieces of information about managed servers by ansible. It allows you to create a group of servers that make you more easier to manage and scale the inventory file itself. The inventory file can be created with many different formats, including the INI and YAML formats.

#### site.yml 
- The master playbook file that contains which group of hosts that will be managed using our available roles.

#### roles 
- it's a group of Ansible playbooks that will be used to provision the server. The ansible roles have their own directory structures, each role will contain directories such as tasks, handlers, vars etc.

## Basic Setup

#### `/roles`
- will hold our ansible roles. 
Roles are a way of grouping similiar tasks together.

- Populate 3 roles for our playbook via `ansible-galaxy`
  - `cd roles`
  - `ansible-galaxy init common`
  - `ansible-galaxy init web`
  - `ansible-galaxy init db`

#### hosts

Edit the `./hosts` file and add the following.

```
[lemp]
server01 ansible_host=10.5.5.26
```

#### `sites.yml`
edit your `./sites.yml`

```
---

- hosts: lemp
  remote_user: mdupree
  become: yes

  roles:
    - common
    - web
    - db

```
## Roles Configuration

Roles are a way to group similiar tasks or playbooks together.

### Common Roles (setup)

This is a list of tasks we are going to perform on the 'common' roles
- Change repo
- Update repo
- Upgrade packages to the latest version
- Setup the server timezone.

Edit `./roles/common/tasks/main.yml` and add the following.

- Create a task for changing the repo.

``` yml
- name: Change the repo Ubuntu 18.04 (setup web roles)
  copy:
    src: sources.list
    dest: /etc/apt/
    backup: yes
```

- Create a task for updating the repository and upgrade all the packages to their latest version using the ansible built in `apt` module. (which is a handler for the debian apt package manager)
``` yml
- name: Update repository and upgrade packages
  apt:
    upgrade: dist
    update_cache: yes
```

- Create a task for configuring the system timezone using the ansible timezone module.
``` yml
- name: Setup timezone to America/Los_Angeles
  timezone:
    name: America/Los_Angeles
    state: latest
```

Now create the `sources.list` file within `./roles/common/files`

`touch ./roles/common/files/sources.list`

```
deb http://buaya.klas.or.id/ubuntu/ bionic main restricted
deb http://buaya.klas.or.id/ubuntu/ bionic-updates main restricted
deb http://buaya.klas.or.id/ubuntu/ bionic universe
deb http://buaya.klas.or.id/ubuntu/ bionic-updates universe
deb http://buaya.klas.or.id/ubuntu/ bionic multiverse
deb http://buaya.klas.or.id/ubuntu/ bionic-updates multiverse
deb http://buaya.klas.or.id/ubuntu/ bionic-backports main restricted universe multiverse
deb http://buaya.klas.or.id/ubuntu/ bionic-security main restricted
deb http://buaya.klas.or.id/ubuntu/ bionic-security universe
deb http://buaya.klas.or.id/ubuntu/ bionic-security multiverse
```

Now the `common` roles config has been completed.

### Web Roles

Now let's create the web role which will do some tasks including install Nginx, Php fpm.

The tasks we are going to perfom are:
- Install `nginx`
- Install `php-fpm`
- Configure `php.ini`
- Create a virtual host
- Add file phpinfo

Enter the web roles directory

`cd ./roles/web/tasks`

Edit `./roles/web/tasks/main.yml`

Create task for the nginx install
``` yml
- name: Install Nginx
  apt:
    name: nginx
    state: latest
```

Now create a task for php-fpm

``` yml
- name: Install php and things
  apt:
    name: ['php','php-fpm','php-common','php-cli','php-curl']
    state: latest
```

Now create the task to configure `php.ini` we will use ansibles blockinfile module. At the end we will notify the system to restart php-fpm.
``` yml
- name: Configure php.ini
  blockinfile:
    dest: /etc/php/{{ php_version }}/fpm/php.ini
    block: |
      date.time = Asia/Jakarta
      cgi-fix_pathinfo = 0
    backup: yes
  notify: restart php-fpm
```

Next we will create a task that uses template module which will copy the configuration from the `templates/` directory to the remote server. Copy example host template `example.j2` to the `/etc/nginx/sites-enabled` directory and lastly restart the service.

``` yml
- name: Create Nginx virtual host
  template:
    src: vhost.j2
    dest: /etc/nginx/sites-enabled/vhost-{{ domain_name }}
  notify: restart nginx
```

Lastly, create task for creating the web-root directory. Use the file module and copy the index.php template into it.

```yml
- name: Create web-root directory
  file:
    path: /var/www/{{ domain_name }}
    state: directory

- name: Upload index.html and info.php files
  template:
    src: index.php.j2
    dest: /var/www/{{ domain_name }}/index.php
```

#### Define handlers
> for services that need reloading

Edit `./roles/web/handlers/main.yml`

``` yml
- name: restart nginx
  service:
    name: nginx
    state: restarted
    enabled: yes

- name: restart php-fpm
  service:
    name: php{{ php_version }}-fpm
    state: restarted
    enabled: yes
```

#### Create the variables

Edit `./roles/web/vars/main.yml`

``` yml
php_version: 7.2
domain_name: example-site.ca
```

#### Create the templates

Create the template for the page.

Edit `./roles/web/templates/index.php.j2`

``` html php
<html>
  <body>
    <h1>
      <center>
        index.html for domain {{ domain_name }
      </center>
    </h1>
    <?php phpinfo(); ?>
  </body>
</html>
```

Create the template for virtual host.

`./roles/web/templates/vhost.j2`

``` nginx
server {
    listen 80;
    listen [::]:80;

    root /var/www/{{ domain_name }};
    index index.php index.html index.htm index.nginx-debian.html;

    server_name {{ domain_name }};

    location / {
        try_files $uri $uri/ =404;
    }

    # pass PHP scripts to FastCGI server
    #
        location ~ \.php$ {
            try_files $uri =404;
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_pass unix:/var/run/php/php{{ php_version }}-fpm.sock;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
        }

}
```

### DB Role

configure the db role for MySQL.

- Install mysql
- Create MySQL database
- Create MySQL user
- Restart mysql

Enter the DB Roles directory. `./roles/db/`

Edit `./roles/db/tasks/main.yml`
``` yml
- name: Install MySQL
  apt:
    name: ['mysql-server','mysql-client','python-mysqldb']
    state: latest
  notify: restart mysql

- name: Create database
  mysql_db:
    name: '{{ db_name }}'
    state: present

- name: Create user for the database
  mysql_user:
    name: '{{ db_user }}'
    password: '{{ db_pass }}'
    encrypted: yes
    priv: '{{ db_name }}.*:ALL'
    state: present
```
Edit `./roles/db/handlers/main.yml`
``` yml
- name: restart mysql
  service:
    name: mysql
    state: restarted
    enabled: yes
```

Edit `./roles/db/vars/main.yml`

``` yml
db_name: <EDIT THIS WITH YOUR DB NAME>
db_user: <EDIT THIS WITH YOUR DB USERNAME>
db_pass: <EDIT THIS WITH YOUR DB PASSWORD>
```

## Run the playbook

From the project root.

With the CHECK flag. Dry Run

`ansible-playbook -i hosts site.yml -c` 

IF all looks good, run it for real

`ansible-playbook -i hosts site.yml`
